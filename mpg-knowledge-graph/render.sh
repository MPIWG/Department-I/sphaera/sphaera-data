#!/bin/bash
source .env
curl -H 'Accept: text/plain' --data-urlencode 'query@mapping.sparql' $ENDPOINT > output/dump.nt
rapper -i turtle -o ntriples project.ttl >> output/dump.nt