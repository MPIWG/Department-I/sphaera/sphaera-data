# Additional Data

## viaf_concordances.csv

Lists VIAF identifiers for those Person entities in the Sphaera database that had, at the time of publishing, no Wikidata identifier. This dataset is used to generate new Wikidata entities as required
