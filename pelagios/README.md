# About

A mapping and version of the Sphaera database to be ingested into [Pelagios](http://pelagios.org/). Specifications can be found here: https://github.com/pelagios/pelagios-cookbook/wiki/Joining-Pelagios. The relevant information is copied below:



# The Dataset Summary

The dataset summary needs to be formatted according to a specific syntax, and be encoded as [RDF](http://en.wikipedia.org/wiki/Resource_Description_Framework), the "markup language of the Semantic Web". But no need to panic. Getting the syntax right is less complicated than it sounds, and we have some examples to guide you.

What _may_ cost you some time, however, is that you must __align your place references with a gazetteer__. Gazetteer alignment is at the heart of the Pelagios network. By using the same "vocabulary" when referring to places, you are implicitly connecting your data with other datasets on the Web, on a global scale.

Depending on the nature of your data, different gazetteers may be suitable as a basis for alignment. At the moment, we support the following gazetteers:

* [Pleiades Gazetteer of the Ancient World](http://pleiades.stoa.org/)
* [Digital Atlas of the Roman Empire](http://imperium.ahlfeldt.se/)
* [Vici.org](http://vici.org/)
* [iDAI - Gazetteer of the German Archaeological Insitute](http://gazetteer.dainst.org/)


## Minimum Example

The RDF snippet below shows a minimum dataset summary example. It lists a single item that is related
to a single place (its findspot). The example is written in [Turtle notation](http://en.wikipedia.org/wiki/Turtle_%28syntax%29), one of the (in our opinion, most human-readable) ways in which RDF can be published.

```
@prefix cnt: <http://www.w3.org/2011/content#> . 
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix oa: <http://www.w3.org/ns/oa#> .
@prefix pelagios: <http://pelagios.github.io/vocab/terms#> .
@prefix relations: <http://pelagios.github.io/vocab/relations#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema> .
 
# An object you want to link to Pelagios
<http://example.org/pelagios/dump.ttl#items/00l>
   a pelagios:AnnotatedThing ;

 # Title and homepage URL are MANDATORY
 dcterms:title "Honorific inscription of Ostia" ;
 foaf:homepage <http://edh-www.adw.uni-heidelberg.de/...> ;

 # Everything else OPTIONAL (but highly encouraged
 dcterms:description "Honorific inscription, findspot Ostia" ;

 # Use ISO 8601 (YYYY[-MM-DD) or time interval (<start>/<end>)
 # to express date information
 dcterms:temporal "366/402" ;

 # Additionally, we encourage the use of (one or multiple)
 # PeriodO identifiers to denote time periods 
 dcterms:temporal <http://n2t.net/ark:/99152/p03wskd389m> ; # Greco-Roman

 # To express an object's language (e.g. in case of literature, 
 # inscriptions, etc.), use RFC 5646 format
 dcterms:language "la" ;

 # Feel free to assign 'tags' to your data
 dcterms:subject "inscription" ;
 .
 

# Objects are 'annotated' with any number of gazetteer references
<http://example.org/pelagios/dump.ttl#items/00l/annotations/01>
   a oa:Annotation ;

 # MANDATORY: the 'annotation target' is the URI of your object;
 # the 'annotation body' is the gazetteer reference
 oa:hasTarget <http://example.org/pelagios/dump.ttl#items/00l> ;
 oa:hasBody <http://pleiades.stoa.org/places/422995> ;

 # OPTIONAL: extra metadata about the nature of the place reference
 pelagios:relation relations:foundAt ;
 oa:hasBody [ cnt:chars "POINT (41.755740099 12.290938199)";
              dcterms:format "application/wkt" ] ;
 oa:annotatedAt "2014-11-05T10:18:00Z"^^xsd:date ;
 .
```


# The VoID File

Once you have all your data set up, mapped your place references to gazetteers, and posted your dataset summary file online, there is one last tiny thing you need: a separate, small RDF file which acts as __entry point to your data__ and contains __global descriptive information__: what's inside the dataset is, who's publishing it, under what license, etc. This file must also contain the __links to all your dataset summary files__, so that they can be harvested easily.

We use the RDF [Vocabulary of Interlinked Datasets (VoID)](http://www.w3.org/TR/void/) to encode this information. A minium, Pelagios-specific example is below.

```
@prefix : <http://my-domain.org/my-data/> .
@prefix void: <http://rdfs.org/ns/void#> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .

:my-dataset a void:Dataset;
  dcterms:title "My Archaeological Dataset";
  dcterms:publisher "My Institution or Project";
  foaf:homepage <https://my-domain.org/>;
  dcterms:description "A dataset of archaeological items.";
  dcterms:license <http://opendatacommons.org/licenses/by/>;

  # This is VERY important
  void:dataDump <http://my-domain.org/downloads/pelagios.ttl> ;
  .
```

