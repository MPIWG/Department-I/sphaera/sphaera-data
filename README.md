## About

This repository contains mappings and exports of the [Sphaera dataset](http://db.sphaera.mpiwg-berlin.mpg.de).

The project is led by [Matteo Valleriani](http://www.mpiwg-berlin.mpg.de/en/users/valleriani) and the repository is maintained by [Florian Kräutli](http://www.mpiwg-berlin.mpg.de/en/users/fkraeutli)
